import React from 'react';
import BlogTemplate from './BlogTemplate';

const BlogPostPreview = ({ entry }) => {
    return (
        <BlogTemplate
            description={entry.getIn(['data', 'description'])}
            title={entry.getIn(['data', 'title'])}
            date={entry.getIn(['data', 'date'])}
        />
    )
}

export default BlogPostPreview;