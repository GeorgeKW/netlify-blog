import React from 'react';

const BlogTemplate = ({
    title,
    description,
    date
}) => {
    return (
        <div>
            <header>
                {title}
            </header>
            <h1>
                {description}
            </h1>
            <h2>
                {date}
            </h2>
        </div>
    )
}

export default BlogTemplate;